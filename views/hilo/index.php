<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\HiloSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hilos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hilo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Hilo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'photo:ntext',
            'like:email',

            ['class' => 'yii\grid\ActionColumn', 'header' => 'عملیات', 'template' => '{update}{delete}{view}'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
