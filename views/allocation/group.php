<?php

use hoomanMirghasemi\jdf\Jdf;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Group Insert';
$jdf = 1;
$startDate = $time;

$next = $startDate + (7 * 86400);
$previous = $startDate - (7 * 86400);


$totalTime1 = 'عدم حضور';
$totalTime2 = 'عدم حضور';
$totalTime3 = 'عدم حضور';
$totalTime4 = 'عدم حضور';
$totalTime5 = 'عدم حضور';
$totalTime6 = 'عدم حضور';
$totalTime7 = 'عدم حضور';

foreach($IOInfo as $key => $value){
    if($value['BEGINDATE'] == Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $startDate))) {
        $totalTime1 = $value['DURATION'];
    }elseif($value['BEGINDATE'] == Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $startDate - 86400))){
        $totalTime2 = $value['DURATION'];
    }elseif($value['BEGINDATE'] == Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $startDate - 86400 * 2))){
        $totalTime3 = $value['DURATION'];
    }elseif($value['BEGINDATE'] == Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $startDate - 86400 * 3))){
        $totalTime4 = $value['DURATION'];
    }elseif($value['BEGINDATE'] == Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $startDate - 86400 * 4))){
        $totalTime5 = $value['DURATION'];
    }elseif($value['BEGINDATE'] == Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $startDate - 86400 * 5))){
        $totalTime6 = $value['DURATION'];
    }elseif($value['BEGINDATE'] == Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $startDate - 86400 * 6))){
        $totalTime7 = $value['DURATION'];
    }
}






?>


<h2>وارد کردن گروهی کارکرد</h2>
<?php $form = ActiveForm::begin(); ?>

<div class="card text-center">
    <div class=" ">

    ‍   <?php 
    if(intval($time*.00001)!=intval(time()*.00001)) {
        
        echo '<a href="'.Url::to(["allocation/group", "endDate" => time()]).'" class="btn btn-warning" style="">&laquo; رفتن به امروز</a>';
    }
    ?>        

        <a href="<?php echo Url::to(['allocation/group', 'endDate' => $previous]); ?>" class="previous">&laquo; هفته قبل</a>
        <a href="<?php echo Url::to(['allocation/group', 'endDate' => $next]); ?>" class="next">هفته بعد  &raquo;</a>


    </div>
</div>



<?php ActiveForm::end(); ?>

<?php if (Yii::$app->session->hasFlash('percent')): ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>

        <?= Yii::$app->session->getFlash('percent') ?>
    </div>
<?php endif; ?>
<div class="table-responsive">
    <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th>#</th>
                <th>نام پروژه</th>
                <th>کد پروژه</th>
                <th>فاز</th>
                <th><?php echo Jdf::jdate('Y/n/j', $startDate).'<br>'.Yii::$app->mycomponent->convertdate( $startDate).'<br>'.$totalTime1; ?></th>
                <th><?php echo Jdf::jdate('Y/n/j', $startDate - 86400 * 1).'<br>'.Yii::$app->mycomponent->convertdate( $startDate - 86400 * 1).'<br>'.$totalTime2; ?></th>
                <th><?php echo Jdf::jdate('Y/n/j', $startDate - 86400 * 2).'<br>'.Yii::$app->mycomponent->convertdate( $startDate - 86400 * 2).'<br>'.$totalTime3; ?></th>
                <th><?php echo Jdf::jdate('Y/n/j', $startDate - 86400 * 3).'<br>'.Yii::$app->mycomponent->convertdate( $startDate - 86400 * 3).'<br>'.$totalTime4; ?></th>
                <th><?php echo Jdf::jdate('Y/n/j', $startDate - 86400 * 4).'<br>'.Yii::$app->mycomponent->convertdate( $startDate - 86400 * 4).'<br>'.$totalTime5; ?></th>
                <th><?php echo Jdf::jdate('Y/n/j', $startDate - 86400 * 5).'<br>'.Yii::$app->mycomponent->convertdate( $startDate - 86400 * 5).'<br>'.$totalTime6; ?></th>
                <th><?php echo Jdf::jdate('Y/n/j', $startDate - 86400 * 6).'<br>'.Yii::$app->mycomponent->convertdate( $startDate - 86400 * 6).'<br>'.$totalTime7; ?></th>
            </tr> 
        </thead>
        <tbody>
        <form method="post" action="#">

            <input name="endTime" type="hidden" value="<?= $startDate; ?>"/>
            <?php
            $x = 0;
            foreach ($data as $item) {
                $day = [];
                $x = 0;
                foreach ($defaultData as $row) {
                    $x++;
                    if ($row['level_projectID'] == $item['level']) {
                        $temp = $row['date'];
                        $temp = substr($temp, 0, 10);
                        $temp = explode("-", $temp);
                        $temp = jdf::jalali_to_gregorian($temp[0], $temp[1], $temp[2]);
                        $your_date = strtotime("$temp[0]-$temp[1]-$temp[2]");
                        $datediff = $startDate - $your_date;
                        $day[round($datediff / (60 * 60 * 24))] = $row['percent'];
                    }
                }

                $nameFeild = "percent" . $item['level'];
                $x++;
                ?>
                <tr>
                    <td>  <?php echo $x; ?></td>
                    <td>  <?php echo $item['name']; ?></td>
                    <td>  <?php echo $item['project_code']; ?></td>
                    <td>  <?php echo $item['level_name']; ?></td>
                    <td> <input class="small input1" onkeyup="compare(this)" name="<?php echo $nameFeild; ?>[]" type="number" value="<?php
                        if (isset($day[1])) {
                            echo($day[1]);
                        } else {
                            echo 0;
                        }
                        ?>"   /><span class="hilo" value="<?php echo $totalTime1; ?>"></span></td>
                    <td> <input class="small input2" onkeyup="compare(this)" name="<?php echo $nameFeild; ?>[]" type="number" value="<?php
                        if (isset($day[2])) {
                            echo($day[2]);
                        } else {
                            echo 0;
                        }
                        ?>"   /><span class="hilo" value="<?php echo $totalTime2; ?>"></span></td>
                    <td> <input class="small input3" onkeyup="compare(this)" name="<?php echo $nameFeild; ?>[]" type="number" value="<?php
                        if (isset($day[3])) {
                            echo($day[3]);
                        } else {
                            echo 0;
                        }
                        ?>"   /><span class="hilo" value="<?php echo $totalTime3; ?>"></span></td>
                    <td> <input class="small input4" onkeyup="compare(this)" name="<?php echo $nameFeild; ?>[]" type="number" value="<?php
                        if (isset($day[4])) {
                            echo($day[4]);
                        } else {
                            echo 0;
                        }
                        ?>"   /><span class="hilo" value="<?php echo $totalTime4; ?>"></span></td>
                    <td> <input class="small input5" onkeyup="compare(this)" name="<?php echo $nameFeild; ?>[]" type="number" value="<?php
                        if (isset($day[5])) {
                            echo($day[5]);
                        } else {
                            echo 0;
                        }
                        ?>"   /><span class="hilo" value="<?php echo $totalTime5; ?>"></span></td>
                    <td> <input class="small input6" onkeyup="compare(this)"  name="<?php echo $nameFeild; ?>[]" type="number" value="<?php
                        if (isset($day[6])) {
                            echo($day[6]);
                        } else {
                            echo 0;
                        }
                        ?>"   /><span class="hilo" value="<?php echo $totalTime6; ?>"></span></td>
                    <td> <input class="small input7" onkeyup="compare(this)" name="<?php echo $nameFeild; ?>[]" type="number" value="<?php
                        if (isset($day[7])) {
                            echo($day[7]);
                        } else {
                            echo 0;
                        }
                        ?>"   /><span class="hilo" value="<?php echo $totalTime7; ?>"></span></td>
                </tr>
                <?php
            }
            ?>

            <hr/>
            <br/>

            <input class="btn btn-info" type="submit" value='ثبت' />
        </form>

        </tbody>
    </table>
</div>

</main>
</div>
</div>

<script>
function compare(l) {
    // if(l.nextElementSibling.getAttribute('value') !== 'عدم حضور') {
    //     let totaltimeee = l.nextElementSibling.getAttribute('value');
    // }else {
    //     let totaltimeee = '0';
    // }
    if(isNaN(l.nextElementSibling.getAttribute('value')*(l.value/100))){
        l.nextElementSibling.innerHTML = 'عدم حضور';
        alert('رکوردی برای حضور شما در این روز ثبت نشده.لطفا از وارد کردن ماموریت و ... خود در این روز اطمینان حاصل کنید.');
    }else if(l.value>100) {
        alert('درصد ورودیه شما بزرگتر از ۱۰۰ میباشد و ثبت این عدد امکان پذیر نمیباشد . لطفا فیلد ورودی را تصحیح کنید و با تشکر تیم نرم افزار');
        l.value = 0;
    }else {
        l.nextElementSibling.innerHTML = Math.floor(l.nextElementSibling.getAttribute('value')*(l.value/100)) + 'min';        
    }
    // l.nextElementSibling.innerHTML = Math.floor(l.nextElementSibling.getAttribute('value')*(l.value/100)) + 'min';

    // alert (l.nextSibling.hilo);
    // let vall = document.getElementByClassName('input'+l.toString())[0].value;
    // alert(l.value*(<?php //echo intval($totalTime7);?>/100));
}


// $(".input1").keyup(funection(){
//     console.log(parseInt($(".input1").val())*(<?php //echo $totalTime1 ?>/100));
// });
// $(".input2").change(funection(){

// });
// $(".input3").change(funection(){

// });
// $(".input4").change(funection(){

// });
// $(".input5").change(funection(){

// });
// $(".input6").change(funection(){
//     allert('dsdada');

// });
// $(".input7").keyup(funection(){
//     $(".input7").css('background','pink')
//     // alert(parseInt($(".input1").val())*(/100));
// });
</script>