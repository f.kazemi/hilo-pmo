<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Hilo */
?>
<div class="hilo-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'photo:ntext',
            'like',
        ],
    ]) ?>

</div>
