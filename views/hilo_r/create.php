<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Hilo */

?>
<div class="hilo-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
