/*
Navicat MySQL Data Transfer

Source Server         : avinManage
Source Server Version : 80020
Source Host           : localhost:3306
Source Database       : avinManage

Target Server Type    : MYSQL
Target Server Version : 80020
File Encoding         : 65001

Date: 2020-09-23 11:40:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for allocation
-- ----------------------------
DROP TABLE IF EXISTS `allocation`;
CREATE TABLE `allocation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `profileID` int DEFAULT NULL,
  `deleted` smallint DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profileID` (`profileID`),
  CONSTRAINT `allocation_ibfk_1` FOREIGN KEY (`profileID`) REFERENCES `profile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of allocation
-- ----------------------------
INSERT INTO `allocation` VALUES ('180', '1399-06-12 11:40:07', '389', null, null);
INSERT INTO `allocation` VALUES ('181', '1399-06-11 11:40:07', '389', null, null);
INSERT INTO `allocation` VALUES ('182', '1399-06-10 11:40:07', '389', null, null);
INSERT INTO `allocation` VALUES ('183', '1399-06-09 11:40:07', '389', null, null);
INSERT INTO `allocation` VALUES ('184', '1399-06-08 11:40:07', '389', null, null);
INSERT INTO `allocation` VALUES ('185', '1399-06-07 11:40:07', '389', null, null);
INSERT INTO `allocation` VALUES ('186', '1399-06-06 11:40:07', '389', null, null);
INSERT INTO `allocation` VALUES ('187', '1399-06-05 11:40:16', '389', null, null);

-- ----------------------------
-- Table structure for allocation_detail
-- ----------------------------
DROP TABLE IF EXISTS `allocation_detail`;
CREATE TABLE `allocation_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `duration` int DEFAULT NULL,
  `allocationID` int DEFAULT NULL,
  `level_projectID` int DEFAULT NULL,
  `percent` smallint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `allocationID` (`allocationID`),
  KEY `level_projectID` (`level_projectID`),
  CONSTRAINT `allocation_detail_ibfk_1` FOREIGN KEY (`allocationID`) REFERENCES `allocation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `allocation_detail_ibfk_2` FOREIGN KEY (`level_projectID`) REFERENCES `level_project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=534 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of allocation_detail
-- ----------------------------
INSERT INTO `allocation_detail` VALUES ('520', null, '183', '34', '10');
INSERT INTO `allocation_detail` VALUES ('521', null, '185', '34', '8');
INSERT INTO `allocation_detail` VALUES ('522', null, '184', '34', '9');
INSERT INTO `allocation_detail` VALUES ('523', null, '182', '34', '12');
INSERT INTO `allocation_detail` VALUES ('524', null, '181', '34', '13');
INSERT INTO `allocation_detail` VALUES ('525', null, '186', '34', '7');
INSERT INTO `allocation_detail` VALUES ('526', null, '187', '34', '6');
INSERT INTO `allocation_detail` VALUES ('527', null, '187', '36', '6');
INSERT INTO `allocation_detail` VALUES ('528', null, '181', '36', '12');
INSERT INTO `allocation_detail` VALUES ('529', null, '182', '36', '11');
INSERT INTO `allocation_detail` VALUES ('530', null, '183', '36', '11');
INSERT INTO `allocation_detail` VALUES ('531', null, '184', '36', '23');
INSERT INTO `allocation_detail` VALUES ('532', null, '185', '36', '21');
INSERT INTO `allocation_detail` VALUES ('533', null, '186', '36', '7');

-- ----------------------------
-- Table structure for auth_assignment
-- ----------------------------
DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `idx-auth_assignment-user_id` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_assignment
-- ----------------------------
INSERT INTO `auth_assignment` VALUES ('admin', '6', '1595237876');
INSERT INTO `auth_assignment` VALUES ('profile', '6', '1595756918');

-- ----------------------------
-- Table structure for auth_item
-- ----------------------------
DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_item
-- ----------------------------
INSERT INTO `auth_item` VALUES ('admin', '1', 'adminstrator', null, null, '1', '1');
INSERT INTO `auth_item` VALUES ('profile', '1', null, null, null, '1595748699', '1595748699');

-- ----------------------------
-- Table structure for auth_item_child
-- ----------------------------
DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_item_child
-- ----------------------------

-- ----------------------------
-- Table structure for auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_rule
-- ----------------------------

-- ----------------------------
-- Table structure for availability
-- ----------------------------
DROP TABLE IF EXISTS `availability`;
CREATE TABLE `availability` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_workID` int DEFAULT NULL,
  `levelID` int DEFAULT NULL,
  `projectID` int DEFAULT NULL,
  `type` smallint DEFAULT NULL,
  `profileID` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_workID` (`category_workID`),
  KEY `projectID` (`projectID`),
  KEY `profileID` (`profileID`),
  KEY `levelID` (`levelID`),
  CONSTRAINT `availability_ibfk_2` FOREIGN KEY (`category_workID`) REFERENCES `work_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `availability_ibfk_4` FOREIGN KEY (`projectID`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `availability_ibfk_5` FOREIGN KEY (`profileID`) REFERENCES `profile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `availability_ibfk_6` FOREIGN KEY (`levelID`) REFERENCES `level_project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of availability
-- ----------------------------
INSERT INTO `availability` VALUES ('51', '10', '34', '4', '2', null);
INSERT INTO `availability` VALUES ('57', null, '34', '4', '1', '326');
INSERT INTO `availability` VALUES ('58', null, '36', '5', '1', '389');

-- ----------------------------
-- Table structure for estimate_datails
-- ----------------------------
DROP TABLE IF EXISTS `estimate_datails`;
CREATE TABLE `estimate_datails` (
  `id` int NOT NULL AUTO_INCREMENT,
  `percent` int DEFAULT NULL,
  `work_levelID` int DEFAULT NULL,
  `estimate_categoryID` int DEFAULT NULL,
  `profileID` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `work_levelID` (`work_levelID`),
  KEY `estimate_categoryID` (`estimate_categoryID`),
  KEY `profileID` (`profileID`),
  CONSTRAINT `estimate_datails_ibfk_2` FOREIGN KEY (`estimate_categoryID`) REFERENCES `estimate_date` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `estimate_datails_ibfk_3` FOREIGN KEY (`profileID`) REFERENCES `profile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estimate_datails
-- ----------------------------

-- ----------------------------
-- Table structure for estimate_date
-- ----------------------------
DROP TABLE IF EXISTS `estimate_date`;
CREATE TABLE `estimate_date` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` smallint DEFAULT NULL,
  `profileID` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profileID` (`profileID`),
  CONSTRAINT `estimate_date_ibfk_1` FOREIGN KEY (`profileID`) REFERENCES `profile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estimate_date
-- ----------------------------

-- ----------------------------
-- Table structure for level_project
-- ----------------------------
DROP TABLE IF EXISTS `level_project`;
CREATE TABLE `level_project` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` text,
  `level` tinyint DEFAULT NULL,
  `projectID` int DEFAULT NULL,
  `parentID` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projectID` (`projectID`),
  KEY `parentID` (`parentID`),
  CONSTRAINT `level_project_ibfk_1` FOREIGN KEY (`projectID`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `level_project_ibfk_2` FOREIGN KEY (`parentID`) REFERENCES `level_project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of level_project
-- ----------------------------
INSERT INTO `level_project` VALUES ('34', 'نرم افزار', '1', '4', null);
INSERT INTO `level_project` VALUES ('36', 'نرم افزار', '1', '5', null);

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', '1592818551');
INSERT INTO `migration` VALUES ('m140209_132017_init', '1592818589');
INSERT INTO `migration` VALUES ('m140403_174025_create_account_table', '1592818590');
INSERT INTO `migration` VALUES ('m140504_113157_update_tables', '1592818590');
INSERT INTO `migration` VALUES ('m140504_130429_create_token_table', '1592818590');
INSERT INTO `migration` VALUES ('m140506_102106_rbac_init', '1594543165');
INSERT INTO `migration` VALUES ('m140830_171933_fix_ip_field', '1592818590');
INSERT INTO `migration` VALUES ('m140830_172703_change_account_table_name', '1592818590');
INSERT INTO `migration` VALUES ('m141222_110026_update_ip_field', '1592818590');
INSERT INTO `migration` VALUES ('m141222_135246_alter_username_length', '1592818590');
INSERT INTO `migration` VALUES ('m150614_103145_update_social_account_table', '1592818590');
INSERT INTO `migration` VALUES ('m150623_212711_fix_username_notnull', '1592818590');
INSERT INTO `migration` VALUES ('m151218_234654_add_timezone_to_profile', '1592818590');
INSERT INTO `migration` VALUES ('m160929_103127_add_last_login_at_to_user_table', '1592818590');
INSERT INTO `migration` VALUES ('m170907_052038_rbac_add_index_on_auth_assignment_user_id', '1594543165');
INSERT INTO `migration` VALUES ('m180523_151638_rbac_updates_indexes_without_prefix', '1594543165');

-- ----------------------------
-- Table structure for profile
-- ----------------------------
DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `id` int NOT NULL AUTO_INCREMENT,
  `personal_code` text,
  `name` text,
  `last_name` text,
  `category_workID` int DEFAULT NULL,
  `userID` int DEFAULT NULL,
  `finish_work` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `category_workID` (`category_workID`),
  KEY `userID` (`userID`),
  CONSTRAINT `profile_ibfk_2` FOREIGN KEY (`category_workID`) REFERENCES `work_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `profile_ibfk_3` FOREIGN KEY (`userID`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=412 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of profile
-- ----------------------------
INSERT INTO `profile` VALUES ('322', '100000', 'شهرام               ', 'سخنور                         ', null, '277', null);
INSERT INTO `profile` VALUES ('323', '960100', 'كاوه                ', 'كياني                         ', null, '278', null);
INSERT INTO `profile` VALUES ('324', '960101', 'زهره                ', 'حسين پور                      ', '11', '279', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('325', '960102', 'زينب                ', 'نامداري                       ', '18', '280', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('326', '960103', 'فرحناز              ', 'فرهودي                        ', '14', '281', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('327', '960104', 'نازيا               ', 'مستعلي                        ', null, '282', null);
INSERT INTO `profile` VALUES ('328', '960105', 'مهدي                ', 'ملكي                          ', null, '283', null);
INSERT INTO `profile` VALUES ('329', '960106', 'احسان               ', 'زنجاني                        ', null, '284', null);
INSERT INTO `profile` VALUES ('330', '960107', 'كوروش               ', 'نعليني                        ', null, '285', null);
INSERT INTO `profile` VALUES ('331', '960108', 'آتوسا               ', 'محمدرضايي                     ', null, '286', null);
INSERT INTO `profile` VALUES ('332', '960109', 'هادي                ', 'حاصلي                         ', null, '287', null);
INSERT INTO `profile` VALUES ('333', '960110', 'حسن                 ', 'جهاني كوهساره                 ', '11', '288', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('334', '960111', 'شاداب               ', 'وثوقي                         ', null, '289', null);
INSERT INTO `profile` VALUES ('335', '960112', 'مهشيد               ', 'مظاهري فر                     ', null, '290', null);
INSERT INTO `profile` VALUES ('336', '960113', 'حسين                ', 'دستاويز                       ', null, '291', null);
INSERT INTO `profile` VALUES ('337', '960114', 'علي                 ', 'كاظم اشرفي                    ', '11', '292', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('338', '960115', 'مسعود               ', 'صمدزاده                       ', null, '293', null);
INSERT INTO `profile` VALUES ('339', '960116', 'محمدحسين            ', 'صابونچي                       ', '13', '294', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('340', '960117', 'زهرا                ', 'شريفيان مزرعه ملائي           ', null, '295', null);
INSERT INTO `profile` VALUES ('341', '960118', 'محمدامين            ', 'شكوهيان                       ', null, '296', null);
INSERT INTO `profile` VALUES ('342', '960119', 'زينب                ', 'مشفق                          ', '18', '297', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('343', '960120', 'شيدا                ', 'نوعي                          ', null, '298', null);
INSERT INTO `profile` VALUES ('344', '960121', 'شيوا                ', 'نوعي                          ', null, '299', null);
INSERT INTO `profile` VALUES ('345', '960122', 'نجمه                ', 'زرنشان                        ', null, '300', null);
INSERT INTO `profile` VALUES ('346', '960123', 'حسين                ', 'عزيزآبادي فراهاني             ', '10', '301', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('347', '960124', 'آتوسا               ', 'خاوريان                       ', '8', '302', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('348', '960125', 'لارا                ', 'صادقي                         ', null, '303', null);
INSERT INTO `profile` VALUES ('349', '960126', 'محمدمهدي            ', 'شكوهي فر                      ', null, '304', null);
INSERT INTO `profile` VALUES ('350', '960127', 'اسماعيل             ', 'صلاحي پروين                   ', null, '305', null);
INSERT INTO `profile` VALUES ('351', '960128', 'جمشيد               ', 'لك                            ', null, '306', null);
INSERT INTO `profile` VALUES ('352', '960129', 'محسن                ', 'احمديان                       ', null, '307', null);
INSERT INTO `profile` VALUES ('353', '960130', 'محمدعلي             ', 'لطفي                          ', null, '308', null);
INSERT INTO `profile` VALUES ('354', '960131', 'مينا                ', 'رضائيان                       ', null, '309', null);
INSERT INTO `profile` VALUES ('355', '960132', 'مريم                ', 'ميرزاپور                      ', '14', '310', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('356', '960133', 'ليلا                ', 'نبي نژاد                      ', '14', '311', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('357', '960134', 'شبنم                ', 'ودايع خيري                    ', '18', '312', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('358', '960135', 'حامد                ', 'حاصلي                         ', null, '313', null);
INSERT INTO `profile` VALUES ('359', '960136', 'بهروز               ', 'صمدزاده                       ', '11', '314', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('360', '960137', 'رضا                 ', 'ايزدي                         ', '8', '315', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('361', '960138', 'سيد حميدرضا         ', 'حسيني                         ', null, '316', null);
INSERT INTO `profile` VALUES ('362', '960139', 'مريم                ', 'علائي                         ', null, '317', null);
INSERT INTO `profile` VALUES ('363', '960140', 'فاطمه               ', 'جعفرخان                       ', '13', '318', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('364', '960141', 'فاطمه سادات         ', 'بي بي شهربانوئي               ', null, '319', null);
INSERT INTO `profile` VALUES ('365', '960142', 'آيدا                ', 'لائي                          ', '10', '320', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('366', '960143', 'نيلوفر              ', 'حكاكي                         ', null, '321', null);
INSERT INTO `profile` VALUES ('367', '960144', 'نيما                ', 'نبوي                          ', null, '322', null);
INSERT INTO `profile` VALUES ('368', '960145', 'مجتبي               ', 'نصيري                         ', '13', '323', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('369', '960146', 'ساجده               ', 'نوري                          ', null, '324', null);
INSERT INTO `profile` VALUES ('370', '960147', 'پگاه                ', 'جعفري                         ', null, '325', null);
INSERT INTO `profile` VALUES ('371', '960148', 'زيبا                ', 'سليمي                         ', null, '326', null);
INSERT INTO `profile` VALUES ('372', '960149', 'زهرا                ', 'صابري                         ', null, '327', null);
INSERT INTO `profile` VALUES ('373', '960150', 'نازنين              ', 'باباخاني                      ', '18', '328', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('374', '960151', 'عليرضا              ', 'صبايي                         ', '17', '329', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('375', '960152', 'زهرا                ', 'صادقي مالواجردي               ', null, '330', null);
INSERT INTO `profile` VALUES ('376', '960153', 'ميلاد               ', 'ملك محمدي نوري                ', null, '331', null);
INSERT INTO `profile` VALUES ('377', '960154', 'محمد                ', 'ملكي كيان                     ', null, '332', null);
INSERT INTO `profile` VALUES ('378', '960155', 'اميد                ', 'سياه منصور                    ', null, '333', null);
INSERT INTO `profile` VALUES ('379', '960156', 'سميه                ', 'حبيبي سرور                    ', null, '334', null);
INSERT INTO `profile` VALUES ('380', '960157', 'سيامك               ', 'محمودي                        ', null, '335', null);
INSERT INTO `profile` VALUES ('381', '960158', 'سيده زهرا           ', 'قاسم پور مقدم                 ', null, '336', null);
INSERT INTO `profile` VALUES ('382', '960159', 'محمودرضا            ', 'ذاكري قادي                    ', null, '337', null);
INSERT INTO `profile` VALUES ('383', '960160', 'عليرضا              ', 'صفري                          ', null, '338', null);
INSERT INTO `profile` VALUES ('384', '960161', 'هادي                ', 'حبيبي                         ', null, '339', null);
INSERT INTO `profile` VALUES ('385', '960163', 'اميررضا             ', 'پوران صفر                     ', null, '340', null);
INSERT INTO `profile` VALUES ('386', '960164', 'نگار                ', 'طلوع ناصري                    ', null, '341', null);
INSERT INTO `profile` VALUES ('387', '960165', 'صابر                ', 'افراسيابي صائين               ', null, '342', null);
INSERT INTO `profile` VALUES ('388', '960166', 'سيد محمدرضا         ', 'بني كريمي                     ', '15', '343', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('389', '960167', 'محمد                ', 'سلطنت پوري                    ', '10', '344', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('390', '960168', 'محمدصادق            ', 'اطهري                         ', null, '345', null);
INSERT INTO `profile` VALUES ('391', '960169', 'آرزو                ', 'گازري نيشابوري                ', '15', '346', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('392', '960170', 'سيد مهدي            ', 'موسوي                         ', null, '347', null);
INSERT INTO `profile` VALUES ('393', '960171', 'مهري                ', 'رباني                         ', '11', '348', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('394', '960172', 'سحر                 ', 'گيتي                          ', null, '349', null);
INSERT INTO `profile` VALUES ('395', '960173', 'زهير                ', 'صادقي                         ', '12', '350', '2020-08-23 14:00:51');
INSERT INTO `profile` VALUES ('396', '960174', 'آرمين               ', 'ايماني                        ', '13', '351', '2020-08-23 14:00:52');
INSERT INTO `profile` VALUES ('397', '960175', 'سامان               ', 'اقبالي                        ', '10', '352', '2020-08-23 14:00:52');
INSERT INTO `profile` VALUES ('398', '960176', 'كيوان               ', 'كريمي                         ', '13', '353', '2020-08-23 14:00:52');
INSERT INTO `profile` VALUES ('399', '960177', 'حسين                ', 'عرفاني                        ', null, '354', null);
INSERT INTO `profile` VALUES ('400', '960178', 'كيكاوس              ', 'اردلان                        ', '11', '355', '2020-08-23 14:00:52');
INSERT INTO `profile` VALUES ('401', '960179', 'قاسم                ', 'جعفري تكاسي                   ', '15', '356', '2020-08-23 14:00:52');
INSERT INTO `profile` VALUES ('402', '960180', 'سهيلا               ', 'جعفرنژاد ثاني                 ', '18', '357', '2020-08-23 14:00:52');
INSERT INTO `profile` VALUES ('403', '960181', 'مسعود               ', 'عسگري مهر                     ', '17', '358', '2020-08-23 14:00:52');
INSERT INTO `profile` VALUES ('404', '960182', 'لويك                ', 'نظريانس                       ', '15', '359', '2020-08-23 14:00:52');
INSERT INTO `profile` VALUES ('405', '960183', 'مهسا                ', 'انوري                         ', '10', '360', '2020-08-23 14:00:52');
INSERT INTO `profile` VALUES ('406', '960184', 'نرگس                ', 'احمدي                         ', '18', '361', '2020-08-23 14:00:52');
INSERT INTO `profile` VALUES ('407', '960185', 'رضا                 ', 'گازري                         ', '10', '362', '2020-08-23 14:00:52');
INSERT INTO `profile` VALUES ('408', '960186', 'ابوالفضل            ', 'زماني                         ', '11', '363', '2020-08-23 14:00:52');
INSERT INTO `profile` VALUES ('409', '9039400', 'فاطمه               ', 'اينانلو شويكلو                ', null, '364', null);
INSERT INTO `profile` VALUES ('410', '1', 'milad', '', null, '6', '2020-08-31 14:51:18');
INSERT INTO `profile` VALUES ('411', '2', null, null, null, '7', null);

-- ----------------------------
-- Table structure for profile_old
-- ----------------------------
DROP TABLE IF EXISTS `profile_old`;
CREATE TABLE `profile_old` (
  `user_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `timezone` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of profile_old
-- ----------------------------

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `start_date` int DEFAULT NULL,
  `end_date` int DEFAULT NULL,
  `active` tinyint DEFAULT NULL,
  `project_code` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES ('4', 'کابین سازمان', null, null, '1', '101');
INSERT INTO `project` VALUES ('5', 'کابین عراق', null, null, '1', '102');
INSERT INTO `project` VALUES ('6', 'کابین مدرسه', null, null, '1', '103');
INSERT INTO `project` VALUES ('7', 'ارائه خدمات فاوا (ICT) جهت پشتیبانی فعالیت های فناوری اطلاعات و ارتباطات', null, null, '1', '104');
INSERT INTO `project` VALUES ('8', 'ممیزی و پایش سیاست نامه های امنیتی', null, null, '1', '105');
INSERT INTO `project` VALUES ('9', 'ساپ', null, null, '1', '106');
INSERT INTO `project` VALUES ('10', 'مشاوره در انتخاب، خرید و استقرار نرم‏افزار مدیریت امور صرافی ', null, null, '1', '107');
INSERT INTO `project` VALUES ('11', 'تدوین طرح تداوم کسب و کار در دامنه خدمات غیرحضوری', null, null, '1', '108');
INSERT INTO `project` VALUES ('12', 'ارزیابی بلوغ و ترسیم نقشه راه معماری فناوری اطلاعات بانک ملت', null, null, '1', '109');
INSERT INTO `project` VALUES ('13', 'تامین نیروی متخصص جهت تدوین نقشه راه استقرار بانکداری دیجیتال در بانک ملت', null, null, '1', '110');
INSERT INTO `project` VALUES ('14', 'تدوین فرایندها و ساختار ارتباطی و گزارش دهی مدیریت ریسک فناوری اطلاعات بانک ملت', null, null, '1', '111');

-- ----------------------------
-- Table structure for social_account
-- ----------------------------
DROP TABLE IF EXISTS `social_account`;
CREATE TABLE `social_account` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `provider` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `code` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_unique` (`provider`,`client_id`),
  UNIQUE KEY `account_unique_code` (`code`),
  KEY `fk_user_account` (`user_id`),
  CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of social_account
-- ----------------------------

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `user_id` int NOT NULL,
  `code` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int NOT NULL,
  `type` smallint NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`),
  CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of token
-- ----------------------------
INSERT INTO `token` VALUES ('6', 'TRdrJC8iHE65WvR2UICLQ-4NDssQuNHB', '1592830018', '0');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int DEFAULT NULL,
  `unconfirmed_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int DEFAULT NULL,
  `registration_ip` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `flags` int NOT NULL DEFAULT '0',
  `last_login_at` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_username` (`username`),
  UNIQUE KEY `user_unique_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=365 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('6', 'admin', 'mysaltern@gmail.com', '$2y$10$AGT3EZ0IY72VqVXTm6qfM.oXEgdv/GhI0g2o0o/gHnuWkZTd68JN6', 'c1rVVnpfBlMSmG8G8DtVlRoK9bb0vYqU', '1', null, null, '127.0.0.1', '1592830018', '1592830018', '0', '1600845921');
INSERT INTO `user` VALUES ('7', 'mysaltern', 'miladjan63@gmail.com', '$2y$12$rPmQbemVxBk98ZjBR7uFvenQG60VBLknsDoZDqKfntStmBcCgUmjm', 'NidzBEHHOoUCLxEK-HrV7-Pe5fkej7Y8', '1595237926', null, null, '127.0.0.1', '1595237926', '1595237926', '0', '1598160809');
INSERT INTO `user` VALUES ('277', '100000', '100000', '$2y$13$.Et3w1Vcj/JQhW3IbV.oteJYOhXh3xQPYd8l9wntmG9N55OvY/p0e', 'Z3umCuBAR15vgW7PJe1j6HALDgnvUI2O', null, null, null, null, '1598160985', '1598160985', '0', null);
INSERT INTO `user` VALUES ('278', '960100', '960100', '$2y$13$4ONCh9Z0w.UW1wmcn4schuCcXuZQSs6Xd7g7H.hcRrfvGdhJIwKa.', 'End5xISlNNVdq7KpFmj52SitSc5P-Az2', null, null, null, null, '1598160985', '1598160985', '0', null);
INSERT INTO `user` VALUES ('279', '960101', '960101', '$2y$13$Ck5DTPGrwhr41zHo5jI1l.Gs/y/qtZNV74vLGuAXlHFys1yRcLqru', 'MGft121RMn1m8Rbjwfbd-W2g8qisONCK', null, null, null, null, '1598160986', '1598160986', '0', null);
INSERT INTO `user` VALUES ('280', '960102', '960102', '$2y$13$Rcqgf5x3kPwoNaxzbdxpRuISIy.Lw4f/tk18a3Qc/kyMRo/LKcL2e', '7d7sb-Dxo0nTeltM7GunfiSeyLESN0D3', null, null, null, null, '1598160986', '1598160986', '0', null);
INSERT INTO `user` VALUES ('281', '960103', '960103', '$2y$13$IL5wVS3L22xiuRalWFjfDueWTH7hQLTEm0EdMS31iUAVyMzv6FJFu', 'XVV_8ocJKV90YesaMDK3ZQ9adbJtw7-0', null, null, null, null, '1598160987', '1598160987', '0', null);
INSERT INTO `user` VALUES ('282', '960104', '960104', '$2y$13$4fSKrWokB2kGYhX1HB8mQe0hC1uzjo9IHZA/SJ1hq5jlwT0zhJ90C', 'FWvEQnJ6nVAN7jeGTYUrR3RAp2lwbK9G', null, null, null, null, '1598160987', '1598160987', '0', null);
INSERT INTO `user` VALUES ('283', '960105', '960105', '$2y$13$hPw9Gtod5Rg5Bh.UJ8WHH.Ug1wp8TE8YN4X9fUxHiDP4kRAflvmSO', 'VOTSBtUbZc70GCcllpaN-OcgKKwgqGLs', null, null, null, null, '1598160988', '1598160988', '0', null);
INSERT INTO `user` VALUES ('284', '960106', '960106', '$2y$13$w.PScZpV6Ar0LAPIRBpLquVjzSgEIVrmj66m90aDC4VHPETfZZKva', 'm6x3Uws9-gpHyRs3UylqhuCRuDvXr3bC', null, null, null, null, '1598160988', '1598160988', '0', null);
INSERT INTO `user` VALUES ('285', '960107', '960107', '$2y$13$s8WjdeM3wMx1rOVVSD9esOtcVJ5bLM2jq8DU2GV8ew2pPb4264QbO', '1ckmmpjQmApG31-Qps_acgUFbhQr59Dr', null, null, null, null, '1598160989', '1598160989', '0', null);
INSERT INTO `user` VALUES ('286', '960108', '960108', '$2y$13$vPq4oxpD1Tyr08J4NjM.z.PPdVB43jacsqzCSKDwbIP0jHprhd94y', 'jCkksIsLMnxYVT_rmbACYbBEk40RkfPt', null, null, null, null, '1598160989', '1598160989', '0', null);
INSERT INTO `user` VALUES ('287', '960109', '960109', '$2y$13$5yTlvYRB5hcD/yENqqr19ORsun.HSs1.R23ru61XR9pniCpUo5F2a', 'uw6KXD5G7BrK0TCGg_tEgKpvKn3IlCnD', null, null, null, null, '1598160990', '1598160990', '0', null);
INSERT INTO `user` VALUES ('288', '960110', '960110', '$2y$13$ORqS24nVTQAlcg//4sSM0evZN5tXa9E7ckZRFiELL64zC8lMBxc6G', 'rExBQJj9ejap4z8ZIfpGoG_90xbCBabi', null, null, null, null, '1598160990', '1598160990', '0', null);
INSERT INTO `user` VALUES ('289', '960111', '960111', '$2y$13$kbTmuc8ltiPwP6HmhOmDQ.XbvrPkcwn5V/uWs6NdszVdkhUhWefw6', 'gkbEcBp2TTSSms9qTtAVXKGTlPO4EK0g', null, null, null, null, '1598160991', '1598160991', '0', null);
INSERT INTO `user` VALUES ('290', '960112', '960112', '$2y$13$CCr7IIVpc3Orl1meIsczoe.mPE1fsUhm3M6Jsr9Ku7M5czhpwM6Oq', '1I551I9Dy5stjMsvlX2KZMkNXpftHrv1', null, null, null, null, '1598160991', '1598160991', '0', null);
INSERT INTO `user` VALUES ('291', '960113', '960113', '$2y$13$v1nT69AQb89xSaQMCF9eiu3l4l5Ivdc48v4QHvPu3DWRn1kY1jqEa', 'TpjmEglDl6jIerePTToCUbliWG2sp-j3', null, null, null, null, '1598160991', '1598160991', '0', null);
INSERT INTO `user` VALUES ('292', '960114', '960114', '$2y$13$17EaiomGCrwoo7CSImwwXOir8CNUOmrE48X23yccIbcp.24ET/4om', 'EuYjJ0gIUv-53fSjOJHOxMN2vQTsVMxC', null, null, null, null, '1598160992', '1598160992', '0', null);
INSERT INTO `user` VALUES ('293', '960115', '960115', '$2y$13$u2TskMiX0vKmQ1krkCD6VeJa1qpna3aWINPd8P6K3CA8Ap8IRPFyK', 'wG-4aIPSwoQFkQQ5Zaiwe7KnEePaEeke', null, null, null, null, '1598160992', '1598160992', '0', null);
INSERT INTO `user` VALUES ('294', '960116', '960116', '$2y$13$d8L6Z/RuNpkic8ZAO7llReI3myMtMXqY.6LAlDoUsXpYJ/S1C2.Lq', 'qZI_utoxpEOUoMBglqKjfhQkhX83bht0', null, null, null, null, '1598160993', '1598160993', '0', null);
INSERT INTO `user` VALUES ('295', '960117', '960117', '$2y$13$fCcZpVEVNPXsy3.9A9J9.e0DMo0TiwWlOFV/LoxBdXS5RUD4GmQk6', 'E-3ci2meaF5nIW_cGcvjmoc8B2LgXt1K', null, null, null, null, '1598160993', '1598160993', '0', null);
INSERT INTO `user` VALUES ('296', '960118', '960118', '$2y$13$78JfvWStuINX5FKU149fA.7ZSB6iqbw7xpY8AVMyXvKqg5pBU2i8a', 'R8FNSg9yDmeZZBqa8aPxEUm2lzwHa-oG', null, null, null, null, '1598160994', '1598160994', '0', null);
INSERT INTO `user` VALUES ('297', '960119', '960119', '$2y$13$/DuaEI08GQHwS54jKB/eW.NeICWWYDrwifc21YKaAuZ1AMSgNMOE.', 'gNZGQDibxBik8LsBPLgzqAMghpC8HsIX', null, null, null, null, '1598160994', '1598160994', '0', null);
INSERT INTO `user` VALUES ('298', '960120', '960120', '$2y$13$3nVoYNSTiL70kSDu5aMpQ.vd1lcZMi3T92XlT1DANdXulkQhCuPq2', '-aGb-DXshJHtlDdFU3domfpYzMKB0PSE', null, null, null, null, '1598160995', '1598160995', '0', null);
INSERT INTO `user` VALUES ('299', '960121', '960121', '$2y$13$ehUDkKC81TyCYO0Yp0ly9.w8pO0kF7KfTGlRjO30F5kq/O3YyWaCG', 'jPyL0P93wtrRW4ekupV97iqc_rDjZCMF', null, null, null, null, '1598160995', '1598160995', '0', null);
INSERT INTO `user` VALUES ('300', '960122', '960122', '$2y$13$MriCVTvI3jP6AP4XglD50e/6llVHOJ7uZy5fs7LNLW6gJeHwKuRpe', 'y_StUizsNHok4t9Ljh51x4NHePYcuyUZ', null, null, null, null, '1598160996', '1598160996', '0', null);
INSERT INTO `user` VALUES ('301', '960123', '960123', '$2y$13$2z/dTps8XxLORqNYCvfdZ.i05H7fRw1phzYJb3FMZBpBfB5snRw6C', 'NuW9YAgfalNXrxS9WvqTm6FyIhHg814V', null, null, null, null, '1598160996', '1598160996', '0', null);
INSERT INTO `user` VALUES ('302', '960124', '960124', '$2y$13$PdFfqLmRTV7XOQ79yQPYwuF.U5FNVIXp7WJRR2te1belkMpnNeW5y', 'ig3GC5HytFnvFF3DEBRPZWo8qP7DcSqy', null, null, null, null, '1598160997', '1598160997', '0', null);
INSERT INTO `user` VALUES ('303', '960125', '960125', '$2y$13$UT66CvI44Gq50MWTR6/M8OO4U1t4l17gW1SYxN9.C7iJDylLTt7oO', 'MEb-eVKXkyHDY7WZrUwGPy5VGq3qb2bv', null, null, null, null, '1598160997', '1598160997', '0', null);
INSERT INTO `user` VALUES ('304', '960126', '960126', '$2y$13$YRMunsl59du.C6WXCu66pu6sUhWHUtSF5dBg06qWQhAstUElLi2za', 'YKInu3_29YM3963BtlvjFhTpTRJUVEYH', null, null, null, null, '1598160998', '1598160998', '0', null);
INSERT INTO `user` VALUES ('305', '960127', '960127', '$2y$13$zf5uTzpSIXJaq0UyGxuIpOjMbEQu4siTmKstggFQB4JctaOLf1gxC', 'BUp21XLoqZ_gVkNiGM0tqTDI68Z5mzSF', null, null, null, null, '1598160998', '1598160998', '0', null);
INSERT INTO `user` VALUES ('306', '960128', '960128', '$2y$13$ngEp.F.MXLKwK5ddVgLffOPHMU.er8bCQHW66QOu.K3sVENRge0xG', '3SQrHYT4EE8QpYjBaidsHcludgBR9Oec', null, null, null, null, '1598160999', '1598160999', '0', null);
INSERT INTO `user` VALUES ('307', '960129', '960129', '$2y$13$6klUrJjM/w6fnoHudbmw7.VsVjnyg6sTrJPvzft9QApItexlU1RXG', 'r44NcSkoPUOD7ZVEwrxdVGWp4TUYAPRv', null, null, null, null, '1598160999', '1598160999', '0', null);
INSERT INTO `user` VALUES ('308', '960130', '960130', '$2y$13$2LmC55OT2vBRTb141Jgo9ek06mQacYC/QJXlkO5vDChzaJDM5Mfdu', 'zNi7pnhxu3x9xc59t5bJ-5fnjKlckeDg', null, null, null, null, '1598161000', '1598161000', '0', null);
INSERT INTO `user` VALUES ('309', '960131', '960131', '$2y$13$DV3yH59vg9R1vO6pSRrZRu/R93to3XRJVTcGbS5QJoT/E3Tzd0KSa', 'YA0yV7uF56b04d9AFDW7JmsnVUzR19is', null, null, null, null, '1598161000', '1598161000', '0', null);
INSERT INTO `user` VALUES ('310', '960132', '960132', '$2y$13$sW/t.pLYUSw5M5WvWy/DSOIIoOif9Gkf0XvbEKYw2x4EEpBmkR2wC', 'WRkEjF3IK2LYmxfBREI1-uB2f8NREKiS', null, null, null, null, '1598161001', '1598161001', '0', null);
INSERT INTO `user` VALUES ('311', '960133', '960133', '$2y$13$yy4wEXMwdzy4ReOkmM1LIuS8BjEKAwoKoel0t1Gh6cH3u/PAzbQ0S', 'DTWpFyQ4tCAPfAg-DRTxNOVpwtIVp_Rd', null, null, null, null, '1598161001', '1598161001', '0', null);
INSERT INTO `user` VALUES ('312', '960134', '960134', '$2y$13$XQSSEL6fHppzooBOCXQleOUPEOwWLCDl1qNKXJ9L48e34VQx5/6bu', 'Je3Z790k-VYP8YL6AZyuVTEQlKz5stEB', null, null, null, null, '1598161002', '1598161002', '0', null);
INSERT INTO `user` VALUES ('313', '960135', '960135', '$2y$13$xyVCRmnu2Yw0zC9Sk9HiBuKj587SaNzDqTJSl0aawr4PyaduIkWvy', '9QVBEAZRs3FwJGC2ii6qAx3HS2MJz2zy', null, null, null, null, '1598161002', '1598161002', '0', null);
INSERT INTO `user` VALUES ('314', '960136', '960136', '$2y$13$ZN4wTAt7NON4GJCqtrgjseizG.k.kV3bGQEBmk0QBJI3VoEWdXP.O', 'kUqOJkQeoQrrTv7FLhjM5Z74ag9oXLTS', null, null, null, null, '1598161003', '1598161003', '0', null);
INSERT INTO `user` VALUES ('315', '960137', '960137', '$2y$13$bMTM4ASq1MnMAxWUW2XmNOZ.gQ/mrCKooSLseOgwUcmkqb6fuCVXG', 'TjjdiJdNaEmNK30UTmEmhKN3q_ccuiQp', null, null, null, null, '1598161003', '1598161003', '0', null);
INSERT INTO `user` VALUES ('316', '960138', '960138', '$2y$13$x2p/h0zv5omrq1m0tKsZFOTnR2dJhvxIFnVP6BuxH3ksDoKhKYfMq', 'EGSpadHaXebTQTbtTe6WvCzhMP9eefrt', null, null, null, null, '1598161004', '1598161004', '0', null);
INSERT INTO `user` VALUES ('317', '960139', '960139', '$2y$13$PT8sderlcPOkQ.kZKPkcR.02QHkY4HZi2dyZk/4pmfARnUAu90vVy', '2WNrFIlwYAAJAxpcqFsKl3H4vmhR8EYy', null, null, null, null, '1598161004', '1598161004', '0', null);
INSERT INTO `user` VALUES ('318', '960140', '960140', '$2y$13$leqvJUsxqESbcSu8FvcGZeZUAiDo4ApBzjTMMkgPwxFd/Lxxzyd4u', 'uxBNWYNcsPIdhbDtM4-zMZa4A6d5IDqT', null, null, null, null, '1598161005', '1598161005', '0', null);
INSERT INTO `user` VALUES ('319', '960141', '960141', '$2y$13$rJ.6vO/yafJ.LsI3V1fIQOLroh99de2quro/qxbqWgAZZCoNWZWwe', '-aoXgXpb8TkLUxP6YQ_8Peiq5KNfmhde', null, null, null, null, '1598161005', '1598161005', '0', null);
INSERT INTO `user` VALUES ('320', '960142', '960142', '$2y$13$T.6oOlAKbVWP5EMquRGJvuGeXkUUirRjCD1aEI9675Mnon3hOgjUC', 'rdMZkO2rGXRA9p6szMvhPYUHZ7S5to5V', null, null, null, null, '1598161006', '1598161006', '0', null);
INSERT INTO `user` VALUES ('321', '960143', '960143', '$2y$13$jWOG7NHWAF9xyKTtd1x5D.7bYLatsnIG4W8uX6ldAVV8an12SwfEC', 'jwPFPYtDzzD2Vf6P16SjHC59CTfoJFNS', null, null, null, null, '1598161006', '1598161006', '0', null);
INSERT INTO `user` VALUES ('322', '960144', '960144', '$2y$13$ZMJiBAl9Bhq7fhcoohKliuDNVNW/lHtqLYIL9l8FCYKcOb258GNze', 'eRBtbNXfIhkJBeTBcbaZmOzYQbf-qKcQ', null, null, null, null, '1598161007', '1598161007', '0', null);
INSERT INTO `user` VALUES ('323', '960145', '960145', '$2y$13$WmhbLgd18HgHR3.hARQlSOT087DZMPrMx4Acq81CCLHfohXRv/r9y', 'J5punVfLI5PK4Y0KMHh1iiHvKRUCRPty', null, null, null, null, '1598161007', '1598161007', '0', null);
INSERT INTO `user` VALUES ('324', '960146', '960146', '$2y$13$8AILf9JmKUcvIBK1WM9Ur.zytzHLlPvaqhvFM5Jqhs7lG3gaX6igK', 'jX6n_QQhdF6fB3F6TA5b6DyCrtgzY-_p', null, null, null, null, '1598161008', '1598161008', '0', null);
INSERT INTO `user` VALUES ('325', '960147', '960147', '$2y$13$J.FSLW0kEF3XhpGL6SUZqO6v3i/aAHzAleuTHXadmlIROlmassw2u', 'HuaExX-lIf2b_ZO32KguYH1MAEsmoip4', null, null, null, null, '1598161008', '1598161008', '0', null);
INSERT INTO `user` VALUES ('326', '960148', '960148', '$2y$13$Xyl0nJFKCb7hcohVM/OSJuypTOT.uiK64tKdsOOipxd1pxWbWfofa', 'FftMW2vQjWj5vX4PMFYmurd535Gumhgq', null, null, null, null, '1598161009', '1598161009', '0', null);
INSERT INTO `user` VALUES ('327', '960149', '960149', '$2y$13$GjxBVu7rHqxLtHx/L9Qb7uKIU5kYwoCX0KXV8ISZDH0nj3FDdL8.S', 'wYmgLFT29Gr6W986Oi20aQPXtVVxvJy3', null, null, null, null, '1598161009', '1598161009', '0', null);
INSERT INTO `user` VALUES ('328', '960150', '960150', '$2y$13$fOiF5Q8gVs9O87kilqeiOeZMZVYZBCCDBBTerUCsHqXMPs7OAgG3i', 'rAy-A4MLsSVgqt7oi4jm1ducWMOeOdRy', null, null, null, null, '1598161010', '1598161010', '0', null);
INSERT INTO `user` VALUES ('329', '960151', '960151', '$2y$13$5CNY9GfFmMor1goVBLz0xeB9w3P.c4dpsTF/kIcQip0Z2qqKHdXgi', '1JuKHtsfXCA2zuj5H6WOvBW9rpoD6Kyw', null, null, null, null, '1598161010', '1598161010', '0', null);
INSERT INTO `user` VALUES ('330', '960152', '960152', '$2y$13$FI.EeEqXC7aG6akPef4ykOR7FOjpv7jINP3xfv1rqAvyblv9viMI6', '0EFdJslGE651eD_rYWvgV-uuu9-MT-fW', null, null, null, null, '1598161011', '1598161011', '0', null);
INSERT INTO `user` VALUES ('331', '960153', '960153', '$2y$13$LTGuZm31r2aSiM/uL8DDp.oqsQVNVb7otbb4TbjUl.IC4YOd2DJoC', 'U-wSBv-bb1zcBsuLruzeRNvjTo-W0Vkc', null, null, null, null, '1598161011', '1598161011', '0', null);
INSERT INTO `user` VALUES ('332', '960154', '960154', '$2y$13$BxxgYCyU2K8QXyiv8gyZeOwnN.gFxRlkWTShsKpITADuquK15GxF.', 'MBd68Xu17mrgqGegWANv3BSqlabWbAxM', null, null, null, null, '1598161012', '1598161012', '0', null);
INSERT INTO `user` VALUES ('333', '960155', '960155', '$2y$13$wqn.xuDbECdRGU/QGm8vfOx5dMN0DDrbSJeyMqroavt9vDh9mVl3m', 'CR3-p0aOK1GaC9yPHJCRIUycXU56-b1R', null, null, null, null, '1598161012', '1598161012', '0', null);
INSERT INTO `user` VALUES ('334', '960156', '960156', '$2y$13$0brx6q2wM.4gnd.Y9KlVDegdkN5BqjDC6gc1.kRpVVb/Ku9Z0yF1i', 'Xlzikby8bY4qKfIrx9HYdjbYNyYP0Eq1', null, null, null, null, '1598161013', '1598161013', '0', null);
INSERT INTO `user` VALUES ('335', '960157', '960157', '$2y$13$IXpJMUn1VrZRRD1GT8MGPOpZKLIZQxrGXFn6/0bDUStUfxseuHEze', 'Nn1vuDKGhUC70kODIbuXMaTMbgwRjrIt', null, null, null, null, '1598161013', '1598161013', '0', null);
INSERT INTO `user` VALUES ('336', '960158', '960158', '$2y$13$mi.ITh9HQqwNnjFBPLGraOPqjQGigDfMvhzNAzkA6/C5cqmFnMHmu', 'PSkLmgJRluQtikKMGOO_GbdFv4ItT5n8', null, null, null, null, '1598161014', '1598161014', '0', null);
INSERT INTO `user` VALUES ('337', '960159', '960159', '$2y$13$G/k3zddTZKcDnrp0SHGHbejyffZH.obJ3cAz4ZOGusW8SFtibeeFG', '9hgigvIhT_yr16m718doT-IEzQyRJVZs', null, null, null, null, '1598161014', '1598161014', '0', null);
INSERT INTO `user` VALUES ('338', '960160', '960160', '$2y$13$fH/Yt.nE9k0DIs8yN2xYT.8QUvC2XtmJSI/TxQuOk0QGH76Luk.pu', 'MJejNVHitwrX-5J8E1jylTQEgEiEqUiL', null, null, null, null, '1598161014', '1598161014', '0', null);
INSERT INTO `user` VALUES ('339', '960161', '960161', '$2y$13$XRAZP4j91UfH/F3WyU52IOJiN9uK0f9KykLhawfd.53r9si5LnZ1K', 'JgB1nVXFWEn1Dd-b36e5qjl46v5AFH1H', null, null, null, null, '1598161015', '1598161015', '0', null);
INSERT INTO `user` VALUES ('340', '960163', '960163', '$2y$13$wKRXBh3J.ooqV5utB4SG4OKUHHvKUnm6gT/M3kd3g8DligSQDzZ8.', 'Z5GFdAIjptYo0mQFOrnXRRGJ1hVQ1tVO', null, null, null, null, '1598161015', '1598161015', '0', null);
INSERT INTO `user` VALUES ('341', '960164', '960164', '$2y$13$RkioVU8pd/S1N1EhilFju.LRV/9B9IG6N7Qp.G3DGz/c9VJhTFBPK', 'ocgZY7Bd24UDPIQBKzl3MkCYPyrekC5i', null, null, null, null, '1598161016', '1598161016', '0', null);
INSERT INTO `user` VALUES ('342', '960165', '960165', '$2y$13$X4G5oIBBKRFqjV25sY7Or.gmxriJcmaa1Jf6pvJKTfbTaCPiPSPIS', 'vU8KMbZ9b3dVRCWwzBun_GrwD-KCZqBd', null, null, null, null, '1598161016', '1598161016', '0', null);
INSERT INTO `user` VALUES ('343', '960166', '960166', '$2y$13$vDmqjxhn7YN9no7d6.miv.XQnZbUCYYabVMrR1fB0jEGf.xkbXN/a', 'hRQsxbbdTl3_AF6OPKzC-WTTsS9hpYXH', null, null, null, null, '1598161017', '1598161017', '0', null);
INSERT INTO `user` VALUES ('344', '960167', '960167', '$2y$13$pv1I7EhbSwTzw0jZCBf1.OkP1tuhuwM1A9VUaT5WQt9lMDymw2cmW', 'KCj7TkbJHj5FwNfpl4bz_YOEULUVnvrY', null, null, null, null, '1598161017', '1598161017', '0', '1599027778');
INSERT INTO `user` VALUES ('345', '960168', '960168', '$2y$13$94O.qcE0F59HFwvrrJL60O4NsVJCf3wU5ru6Ad7r4p4u8GtnoG1xS', '-yZriofgIwVMoDDTkr8Ur1SKn80CzOHH', null, null, null, null, '1598161018', '1598161018', '0', null);
INSERT INTO `user` VALUES ('346', '960169', '960169', '$2y$13$9gpJYObq0M.vyKM2NFJhOuKl0G/i2XeZqs1AsWbIVdSPwMc9aHFRu', 'C25LD7NZN_-OkUfes5gg1EQTcCYJfMrM', null, null, null, null, '1598161018', '1598161018', '0', null);
INSERT INTO `user` VALUES ('347', '960170', '960170', '$2y$13$4GpSu64yKquXVPmOMI9scu3mIs620qSvJCYt4DlhZg6h.CTvtKHmG', '6DstLQ5rEUgZHb6JGctXg7B6Zyv751N-', null, null, null, null, '1598161019', '1598161019', '0', null);
INSERT INTO `user` VALUES ('348', '960171', '960171', '$2y$13$JPujR5qh8lsufV0uHVJpNe4HNbsF4YW2PAU2rWhdPCrv2L7.Xcszq', 'd0mJ5Gc1gcVPdlhphfGTN84t6d5LJxxN', null, null, null, null, '1598161019', '1598161019', '0', null);
INSERT INTO `user` VALUES ('349', '960172', '960172', '$2y$13$3aeu4Uv5xzGSJ2Co02kio.HkcCnP2DXCDm0lRHkU7D549yM.NE2rC', '90dgiat595a6aW4QaTaLETjS_GIz-jNF', null, null, null, null, '1598161020', '1598161020', '0', null);
INSERT INTO `user` VALUES ('350', '960173', '960173', '$2y$13$RMQhhC/IHYBy4mvJXgp6NuxNTHzqTBpVvd3jsnOPkehOrem9khhkS', '7-b7-Iz00XZnOW_SjhMOwxdofY8JY4SC', null, null, null, null, '1598161020', '1598161020', '0', null);
INSERT INTO `user` VALUES ('351', '960174', '960174', '$2y$13$HefuiDcHjf4e14.Rig.syetZFDreb014DZkAeecc03z4DJoyZx9oC', 'RNLRW-ZINVmoDafGz1okhNZ2DuSdgOLg', null, null, null, null, '1598161021', '1598161021', '0', null);
INSERT INTO `user` VALUES ('352', '960175', '960175', '$2y$13$xHbYGeizU1BvlOmhNewrxOFHoI8Oc1pqjCkXbSHlIBDcQ6.eGrtoG', '5dkp_sjBVi_o1AnGlz1-6UHhaX4yp2w4', null, null, null, null, '1598161021', '1598161021', '0', null);
INSERT INTO `user` VALUES ('353', '960176', '960176', '$2y$13$hPdrbwdYn2sRbeVgf20QkuYVwndng3UmS68jB1IbT5wz84p20UX/i', 'd1jzviNtD1h1n7gbKenCqOWMBvS2WhA8', null, null, null, null, '1598161022', '1598161022', '0', null);
INSERT INTO `user` VALUES ('354', '960177', '960177', '$2y$13$/bdUB6lfeaJ2kxvPnJ4D/u4oQbL9MJVuJpoVvUuZDZDZTpsYmdKke', 'VYNh_mrNIHu4OPPXW7O3WZLpnLXkaGuo', null, null, null, null, '1598161022', '1598161022', '0', null);
INSERT INTO `user` VALUES ('355', '960178', '960178', '$2y$13$OOIxrhclK4nqeeb5nhVVguqVndD3IkiaCg0R.WF2aNeNBTd59O1B6', 'ETd-OgUq08nIxEUnRwDMCNz36wRNhiSF', null, null, null, null, '1598161023', '1598161023', '0', null);
INSERT INTO `user` VALUES ('356', '960179', '960179', '$2y$13$ldH/Eh0XX2G/FSSTeWFDnex0A8HTxvWwj3nsZYITh9X3NBWij3WKC', 'LjD5BfqL0hf-UpEKclmQC3ge5glzJDT6', null, null, null, null, '1598161023', '1598161023', '0', null);
INSERT INTO `user` VALUES ('357', '960180', '960180', '$2y$13$SBUpyMuffx2eCnssBdxC5uVUGH3ajhxoUgnjADgxnP6qopneO6FOK', '4mSg3q0M0OiRFFMOgY1RwDruzPkJlwIP', null, null, null, null, '1598161024', '1598161024', '0', null);
INSERT INTO `user` VALUES ('358', '960181', '960181', '$2y$13$h1o0oMhN3wL3Valj3S4V..K0mCYX2USDgPTD1Z/NCjO7YP3LojZvO', '7wFzPVzbYp4epT4AOuBPF1za7Cm_2jHZ', null, null, null, null, '1598161024', '1598161024', '0', null);
INSERT INTO `user` VALUES ('359', '960182', '960182', '$2y$13$fvuDTU.ZcC9EGYWrwqtn6u5EuwJYJX2dqZizneXVfUAsI.20icxcu', 'fB1YUXs89LD5c4EWJoUI03CD8-FNxfQg', null, null, null, null, '1598161025', '1598161025', '0', null);
INSERT INTO `user` VALUES ('360', '960183', '960183', '$2y$13$iT3SqZoJDN3YctJAfFyWVOYKsn8FnKDfty4UTzoZV0gpPcvYv92U.', 'ab8D-lV82smQx5mcGvL2IucMyJ0ig3Sl', null, null, null, null, '1598161025', '1598161025', '0', null);
INSERT INTO `user` VALUES ('361', '960184', '960184', '$2y$13$cKSgbdI/KqmqPcGzfliEcu/RAH19mmOKBOYaAc2JHWBbeKJE9swCe', 'o-rHry2ObjYCwWYY8lGovpfk-dslnuek', null, null, null, null, '1598161026', '1598161026', '0', null);
INSERT INTO `user` VALUES ('362', '960185', '960185', '$2y$13$g4BHlFtavzIk6l3X5fo0yOAs7qjIpOlW57D51SUlfT3kuPC1f62l6', 'kCVwPll4TdOO_kTmyjvr3cfE_zu6Rb46', null, null, null, null, '1598161026', '1598161026', '0', null);
INSERT INTO `user` VALUES ('363', '960186', '960186', '$2y$13$Aaf/ILOSoFtCiKFoA8wITevgITInxqEc1QOHw6t89X7Rk6B2P.eTy', '91Pb_ZhG1RN8d221SXR7Y2m9UmDcRPvN', null, null, null, null, '1598161027', '1598161027', '0', null);
INSERT INTO `user` VALUES ('364', '9039400', '9039400', '$2y$13$U0dppLsEyS19NpksbHkqSePnFv0SlbTErdzTLIiJgBZaxJNy3YwuG', 'L5Z5icpdV9RmI2Y6t70t2PZtNUtxa2PF', null, null, null, null, '1598161027', '1598161027', '0', null);

-- ----------------------------
-- Table structure for work_category
-- ----------------------------
DROP TABLE IF EXISTS `work_category`;
CREATE TABLE `work_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` text,
  `use_default` tinyint DEFAULT NULL,
  `code_category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of work_category
-- ----------------------------
INSERT INTO `work_category` VALUES ('7', 'مدیر', '1', '01');
INSERT INTO `work_category` VALUES ('8', 'واحد ict', '1', '0101');
INSERT INTO `work_category` VALUES ('9', 'واحد توسعه سرمایه انسانی', '1', '0102');
INSERT INTO `work_category` VALUES ('10', 'واحد نرم افزار', '1', '0103');
INSERT INTO `work_category` VALUES ('11', 'واحد مالی و اداری', '1', '0104');
INSERT INTO `work_category` VALUES ('12', 'واحد کسب و کار', '1', '0105');
INSERT INTO `work_category` VALUES ('13', 'واحد ناوگان', '1', '0106');
INSERT INTO `work_category` VALUES ('14', 'واحد کال سنتر', '1', '010601');
INSERT INTO `work_category` VALUES ('15', 'واحد اجرایی', '1', '0107');
INSERT INTO `work_category` VALUES ('16', 'واحد مشاوره', '1', '0108');
INSERT INTO `work_category` VALUES ('17', 'واحد مدیریت', '1', '0109');
INSERT INTO `work_category` VALUES ('18', 'مدیریت درخواست ها', '1', '0110');
SET FOREIGN_KEY_CHECKS=1;
