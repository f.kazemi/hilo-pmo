<?php

namespace app\controllers;

use Yii;
use app\models\AllocationDetail;
use app\models\AllocationDetailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use hoomanMirghasemi\jdf\Jdf;
use yii\filters\AccessControl;
use app\models\Allocation;
use app\models\Profile;
use app\models\IOInfo;

/**
 * AllocationController implements the CRUD actions for AllocationDetail model.
 */
class AllocationController extends Controller {

    public $enableCsrfValidation = false;

    /**
     * {@inheritdoc}
     */

    /** @inheritdoc */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['group'], 'roles' => ['@']],
//                    ['allow' => true, 'actions' => ['show'], 'roles' => ['?', '@']],
                ],
            ],
        ];
    }

    /**
     * Lists all AllocationDetail models.
     * @return mixed
     */
    public function actionIndex() {
            $searchModel = new AllocationDetailSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // die;
            return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
    }

    public function actionReport() {

        $userID = \Yii::$app->user->id;


        return $this->render('report', [
//                    'model' => $this->findModel($id),
        ]);
    }

    public function actionGroup() {


        // die;
        $userID = \Yii::$app->user->id;
        $accessCheck = \app\models\Profile::information($userID);

        $profileID = $accessCheck[0]['profileID'];

        if (isset($_GET['endDate'])) {
            $timeDefault = $_GET['endDate'];
        } else {
            $timeDefault = time();
        }



        $limit = 7;
        $endTime = $timeDefault;


        $data = \app\models\LevelProject::getAccess($profileID, $accessCheck);

        if (isset($_POST)) {

            if ($_POST != null) {

                $infomation = $_POST;

                $endTime = $infomation['endTime'];

                unset($infomation["endTime"]);
                $sumArray = array();

                foreach ($infomation as $k => $subArray) {
                    foreach ($subArray as $id => $value) {
                        if (isset($sumArray[$id])) {
                            $sumArray[$id] = $sumArray[$id] + $value;
                        } else {
                            $sumArray[$id] = $value;
                        }
                    }
                }
                foreach ($sumArray as $checkPercent) {
                    if ($checkPercent > 100) {
                        Yii::$app->session->setFlash('percent', 'درصد یک یا چند روز بیشتر از ۱۰۰ وارد شده است.');
                        $defaultData = Allocation::getWithConditionDetails($endTime, $limit, $profileID);

                        return $this->render('group', [
                                    'data' => $data,
                                    'defaultData' => $defaultData,
                                    'time' => $timeDefault,
                        ]);
                    }
                }

                foreach ($infomation as $key => $value) {


                    $levelProjectID = str_replace('percent', '', $key);
                    $t = 0;

                    foreach ($value as $column) {

                        $t = $t + 1;

                        if ($column !== '0') {



                            $day = Jdf::jdate('Y-n-j', $endTime - ($t * 86400));


                            $day = Yii::$app->mycomponent->convertPersianNumbersToEnglish($day);


                            $check = \app\models\Allocation::getWithCondition($profileID, $day);


                            if (isset($check['id'])) {
                                $idAllocation = $check['id'];
                            } else {

                                $time = date('H:i:s');
                                $allocation = new \app\models\Allocation;
                                $allocation->date = $day . " $time";
                                $allocation->profileID = $profileID;
                                $allocation->save(false);
                                $idAllocation = $allocation->id;
                            }

                            $checkExist = AllocationDetail::checkExist($levelProjectID, $idAllocation);


                            if ($checkExist != false) {
                                $model = AllocationDetail::findOne($checkExist);
                                $model->percent = $column;
                                $status = $model->save(false);
                            } else {
                                $model = new AllocationDetail();
                                $model->level_projectID = $levelProjectID;
                                $model->percent = $column;
                                $model->allocationID = $idAllocation;
                                $status = $model->save(false);
                            }
                        }
                    }
                }
            }
        }


        $model = new Allocation;
        if (isset($_GET['endDate'])) {
            $endTime = $_GET['endDate'];
        }

        $defaultData = Allocation::getWithConditionDetails($endTime, $limit, $profileID);
    // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $userId = Yii::$app->user->id;
      $profileId = Profile::information($userId);
      $profile  =  $profileId[0]['personal_code']; 
    
//                                 $dayy = Yii::$app->mycomponent->convertPersianNumbersToEnglish(Jdf::jdate('n/j', $endTime - 86400 * 11));

      //                            $day = Yii::$app->mycomponent->convertPersianNumbersToEnglish($day);
    //     var_dump( Yii::$app->mycomponent-> convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 1)));
    //   die;
      $IOInfo = IOInfo::find()->where([
            'PERNO' => "    $profile",
        ])->andWhere(['or',
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime))],
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 1))],
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 2))],
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 3))],
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 4))],
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 5))],
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 6))],
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 7))],
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 8))],
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 9))],
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 10))],
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 11))],
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 12))],
            ['BEGINDATE' => Yii::$app->mycomponent->convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 13))]
        ])->asArray()->all();
        // return var_dump (Yii::$app->mycomponent->convertPersianNumbersToEnglish(Jdf::jdate('n/j', $endTime - 86400 * 10)));
    //   return $IOInfo;
    //   return var_dump( '0'.Yii::$app->mycomponent-> convertPersianNumbersToEnglishHILO(Jdf::jdate('n/j', $endTime - 86400 * 1)));
    //     die;
    //     echo '<br>';
    //     echo '<br>';
    //     echo 'this is hilo';
    //     var_dump($data);
    //     echo '<br>';    
    //     echo '<br>';
    //     var_dump($model);
    //     echo '<br>';
    //     echo '<br>';
    //     var_dump($defaultData);
    //     echo '<br>';
    //     echo '<br>';
    //     var_dump($timeDefault);
    //     die;


        return $this->render('group', [
                    'data' => $data,
                    'model' => $model,
                    'defaultData' => $defaultData,
                    'time' => $timeDefault,
                    'IOInfo' => $IOInfo,
                    // 'day' => $day
        ]);
    }

    /**
     * Displays a single AllocationDetail model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AllocationDetail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AllocationDetail();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing AllocationDetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AllocationDetail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AllocationDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AllocationDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AllocationDetail::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
