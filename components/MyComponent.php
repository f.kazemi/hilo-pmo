<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class MyComponent extends Component {

    public function convertTime($time, $format = '%02d:%02d') {

        if ($time < 1) {
            return '00:00';
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    public function count() {
        
    }

    public function arrayForAccess($data) {
        $arr = [];
        $x = 0;

        foreach ($data as $item) {
            $arr[$x]['group'] = $item['group'];
            $arr[$x]['projectID'] = $item['project'];
            $arr[$x]['projectName'] = $item['name'];
            if (isset($item['level'])) {

                $arr[$x]['level'][] = ['id' => $item['level'], 'name' => $item['level_name']];
            } else {
                $allLevel = \app\models\LevelProject::find()->where(['projectID' => $item['project']])->asArray()->all();

                foreach ($allLevel as $row) {
                    //      $arr = $arr + [$row['id'] => $row['name']];

                    $arr[$x]['level'][] = ['id' => $row['id'], 'name' => $row['name']];
                }
            }
            $x++;
        }


        return $arr;
    }

    public function convertPersianNumbersToEnglish($input) {
        $persian = ['۰', '۱', '۲', '۳', '۴', '٤', '۵', '٥', '٦', '۶', '۷', '۸', '۹'];
        $english = [ 0, 1, 2, 3, 4, 4, 5, 5, 6, 6, 7, 8, 9];
        return str_replace($persian, $english, $input);
    }
    public function convertPersianNumbersToEnglishHILO($input) {
        $hil = $this->convertPersianNumbersToEnglish($input);
        // return $input;
        $arar = explode("/",$hil);
        foreach ($arar as $key => $value) {
            if(strlen($value) == 1){
                $arar[$key] = '0'.$value;
            }else{
                $value = '1'.$value;
            }
        }
        $arar_str = implode("/", $arar);
        return $arar_str;
        return $this->convertPersianNumbersToEnglish($arar_str);
    }

    public function convertdate($input) {
        $ddd = date("l", $input);
        $english = [ 'Thursday', 'Wednesday', 'Tuesday', 'Monday', 'Sunday', 'Saturday', 'Friday'];
        $persian = ['<span class="text-danger">پنج شنبه</span>', '<span class="">چهار شنبه</span>', '<span class="">سه شنبه</span>', '<span class="">دو شنبه</span>', '<span class="">یک شنبه</span>', '<span class="">شنبه</span>', '<span class="text-danger">جمعه</span>'];
        return str_replace($english, $persian, $ddd);
        // return $ddd;
    }

}
