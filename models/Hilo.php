<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hilo".
 *
 * @property int $id
 * @property string $name
 * @property string $photo
 * @property int $like
 */
class Hilo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hilo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'photo', 'like'], 'required'],
            [['photo'], 'string'],
            [['like'], 'integer'],
            [['name'], 'string', 'max' => 77],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'آی دی',
            'name' => 'نام',
            'photo' => 'عکس',
            'like' => 'لایک',
        ];
    }
}
